use sqlx::pool::Pool;
use sqlx::{migrate::MigrateDatabase, Row, Sqlite, SqlitePool};
//mod tutorial;
mod util;

const DB_URL: &str = "sqlite://my.db";

async fn print_db(db: &Pool<Sqlite>) {
    let inspect = sqlx::query("SELECT * FROM account")
        .fetch_all(db)
        .await
        .unwrap();

    for (idx, row) in inspect.iter().enumerate() {
        println!("[{}]: {:?}", idx, row.get::<String, &str>("username"));
    }
}

async fn add_item(db: &Pool<Sqlite>) {
    let insert = sqlx::query(
        "INSERT INTO account (username, password, email, create_date, sex, admin)
         VALUES ('wifi lulu', 'abcd1234', 'my.wifi@example.com', current_timestamp, 'F', '0');",
    )
    .execute(db)
    .await
    .unwrap();
    println!("Insert insert result: {:?}", insert);
}

async fn remove_item(db: &Pool<Sqlite>) {
    let insert = sqlx::query("DELETE FROM account WHERE username = 'wifi lulu';")
        .execute(db)
        .await
        .unwrap();
    println!("Insert insert result: {:?}", insert);
}

#[tokio::main]
async fn main() {
    //tutorial::main().await;

    let db = SqlitePool::connect(DB_URL).await.unwrap();

    util::print_type_of(&db);

    remove_item(&db).await;
    add_item(&db).await;
    print_db(&db).await;
}
