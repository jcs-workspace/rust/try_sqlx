/**
 * $File: util.rs $
 * $Date: 2024-05-08 03:02:06 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright © 2024 by Shen, Jen-Chieh $
 */

pub fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>())
}
